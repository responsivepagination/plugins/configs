# Responsive Pagination Plugin Configs

Config files for the free plugin.

### Specification

The config files in this repository supports pagination with this specification:
- Number of total pages : Up to 25 pagination pages
- Device breakpoints : 
	- Small Devices
		- Edge : up to 2 edge items
		- Mid : up to 2 mid items
	- Medium Devices
		- Edge : up to 3 edge items
		- Mid : up to 3 mid items
	- Large Devices
		- Edge : up to 4 edge items
		- Mid : up to 4 mid items


